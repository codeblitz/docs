# Refactoring CodeBlitz Manifesto

**Refactoring CodeBlitz** é uma oficina de revisão e refatoração de código fonte em formato gamificado onde dois grupos de desenvolvedores (**infiltrados** e **não infiltrados**) juntam-se para refatorar, a “quatro mãos”, um determinado código fonte real dentro de um timebox de 2 horas. Os *infiltrados* são os desenvolvedores originais do código em questão enquanto que os *não infiltrados* (**delegado**, **investigador** e **SWAT**) são desenvolvedores experientes, porém externos à equipe, que conduzirão o processo de refatoração. Ao final, o código estará refatorado e os participantes gerarão uma **lista de suspeitos** composta por ações realizadas no evento e ações futuras sobre este mesmo código. A lista pode gerar uma **próxima missão** ou simplesmente itens de backlog técnico. Adicionalmente, é criada uma lista com os **10 mais procurados** contento os 10 principais itens de dívida técnica do sistema. A gameficação é estabelecida por duas pontuações atribuídas aos participantes da missão:
1) Pontuação de **Total de Suspeitos Detidos**: calculada a partir do total de itens refatorados
2) Pontuação de **Variação do Índice de Criminalidade**: calculada a partir da métrica de dívida técnica apurada pela ferramenta de análise estática.

##### Objetivo: Revisar e refatorar um código fonte real com possível identificação de dívida técnica e ações futuras, propiciando troca de experiências, conhecimento, padrões e boas práticas

## Estrutura

##### Participantes (mínimo 3):
- **Infiltrado**: Membro da equipe que mantém o código a ser investigado e refatorado. É responsável por apresentar o sistema e trazer informações privilegiadas sobre o código.
- **Investigador**: Desenvolvedor externo que opera as ferramentas de edição e análise estática. Responsável por narrar e explicar as ações de refatoração em curso.
- **SWAT**: Desenvolvedores externos que atuam diretamente na revisão e refatoração com base nas informações levantadas anteriormente e durante a missão (excluem-se os infiltrados).
	Delegado: Facilitador do evento, responsável pela apresentação das regras, andamento e condução das ações. Pode fazer parte da SWAT, mas não deve ser investigador nem infiltrado.

##### Produtos
- **Lista de Suspeitos**: lista com as partes do código que são suspeitas de estar precisando  de refatoração. Cada item da lista corresponde a uma ação de refatoração sobre o código. Algumas serão realizadas durante o evento e outras ficarão para um momento posterior.
- **10 Mais Procurados**: Lista elaborada ao fim do evento por todos os participantes com os 10 principais itens de dívida técnica ainda não tratados no código e/ou no sistema como um todo.

##### Recursos
- Computador com o código editável e testável + Ferramenta de análise estática
- 1 projetor ou televisão + 2 kits de teclado/mouse (para infiltrados e não infiltrados)


## Dinâmica (01:50  a 2 horas)

### Abertura (5 minutos)
O **delegado** apresenta **objetivo**, **participantes**, e a **dinâmica** do evento.

### Etapa 1 (10 minutos) – Relatório de Campo (Visão Interna)
Os **infiltrados** fazem uma apresentação geral sobre o código que será investigado. Nesta etapa eles já podem apontar suspeitos na **lista de suspeitos** e relatar problemas conhecidos. Os relatos devem ainda incluir um resumo da arquitetura, questões tecnológicas ou outras pertinentes ao evento.

### Etapa 2 (15 minutos) – Investigação e Planejamento (Visão Externa)
A partir do **relatório de campo**, o **investigador** vasculha o código para confirmar as suspeitas dos **infiltrados** e/ou encontrar novos suspeitos. A investigação ocorre de forma expositiva por parte do investigador e pode ter participação da **SWAT** a qualquer momento. 

Ao fim desta etapa, os participantes debatem o que foi levantado e, a partir disso, consolidam e ordenam a **lista de suspeitos** (versão 1) para ser usada na próxima etapa.

### Etapa 3 (60 minutos) – Operação Tática (Refatoração Conjunta)
Com base na ordem da **lista de suspeitos**, iniciam-se as ações de refatoração de código sobre as partes selecionadas. Os membros da **SWAT** revesam-se na operação do computador (teclado/mouse kit 1) para efetuar o início da refatoração acompanhado da explicação do que está sendo feito e porquê. Em seguida os **infiltrados**, também revesando-se (teclado/mouse  kit 2), concluem a refatoração iniciada e esplanada pela **SWAT**. Este ciclo repete-se pelo tempo previsto para a operação. Ao longo de toda a operação, a **lista de suspeitos** pode sofrer adições e/ou modificações bem como nova priorização. Itens não tratados seguem como missão futura. 

### Etapa 4 (10 a 20 Minutos) – Rescaldo (Relatório)
O **investigador** conduz a consolidação e conclusão da **lista de suspeitos**, registrando todos os itens que foram efetivamente resolvidos e os que ficaram para uma oportunidade futura.  A partir deste segundo grupo, de relatos dos **infiltrados** e de observações da **SWAT**, o investigador elabora a lista dos **10 Mais Procurados**.

### Encerramento (10 Minutos) – Retrospectiva
O **delegado** encerra o evento pedindo feedback aos participantes.

## Resultado Esperado
- Código original devidamente refatorado e melhorado
- **Lista de Suspeitos** consolidada com ações realizadas e ações futuras sobre o código
- Troca de experiência, conhecimento, padrões e boas práticas entre os participantes
- Relação dos **10 Mais Procurados** com os 10 principais itens de dívida técnica do sistema

## Pontuação Final da Equipe
1. Total de **suspeitos** detidos (itens refatorados)
2. Variação do índice de **criminalidade** (variação da métrica de dívida técnica)