# CodeBlitz - Pre-Game

The pre-game is an optional (although highly recommended) stage that happens before the actual event. Basically, all of the preliminary activities of understanding and collecting information about the code and the system can be considered part of the pre-game. In some cases, the pre-game is essential to the smooth running of the game. Heterodox architectures and particular implementations can take advantage of a prior treatment of what needs to be discussed during the event. During the pre-game, the **suspect list** can start being crafted with known problems.

You can use different tools to build the list. For instance, if you are using GitLab, you can use the **Issues** as the **suspect list** and link then to parts of the source code or to the issues appointed by a code review tool.

## The **Insiders** pre-game

The original team can do a pre-game gathering documentation, taking note of doubts and suspects about the code and the system as a whole. Real and specific questions are great candidates to be treated at the event. Specific points of the code that the team knows that need refactoring can be listed on the **suspect list** during the pre-game.

## The **Investigators** pre-game

The **Investigators** also should do a pre-game in order to get to event *too fresh*. A good option is to use a code review tool to sort out the main suspects based on the Issues pointed. It's possible that technical debts are discovered during this process. Whatever is found during the pre-game will be validated during the event together with all the participants.

One advantage of using a code review tool is that most of the items based on quality issues do not take much time to be solved, adding to the dynamic of the game and increasing the score of **detained suspects**. Also reduces considerably the time needed to read and understand a source code, as the tool can analyze thousands of lines of code in just a few seconds. The quality issues are great starting points for a human, more detailed and qualitative review.