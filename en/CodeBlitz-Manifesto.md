# CodeBlitz Manifesto

**Refactoring CodeBlitz**  is a workshop for code revision and refactoring where two developer groups (**insiders** and **non-insiders**) join forces to refactor a real-world source code into a fixed 2-hour timebox.
The *insiders* are the original code developers and the *non-insiders* (**deputy**, **investigator** and **SWAT**) are experienced developers, but from other development teams that will conduct the refactoring process. In the end, the code will be refactored and the participants will produce a **suspect list** composed by the actions taken during the event and future action to be taken over the same code. This list can be the start point of a **next mission** or just new items of a technical backlog. Additionally, it's created a list with the **10 most wanted** composed of the systems top 10 technical debt items.
The gamification is defined by two different scores attributed to the mission:
1) **Total Suspects Detained** score: based on the total of refactored items
2) **Crime Rate Variation** score: based on the technical debt metrics provided by some code review tool

##### Goal: Revise and refactor a real-world source code identifying possible technical debt and future actions, allowing the exchange of experiences, knowledge, standards and best practices among developers.

## Structure

##### Participants (minimum of 3):
- **Insider**: Team member that maintains the source code to be investigated and refactored. Responsible for introducing the system and bring qualified information about the code.
- **Investigator**: An external developer that will operate the code review tool. Responsible for explaining and narrate the actions being taken during the refactoring.
- **SWAT**: External developers that will actually review and refactor the code based on the information provided earlier during the mission (excluding the *insiders*).
- **Deputy**: Event's facilitator, responsible for introducing the rules, structure and conducting the actions. Can be part of the *SWAT* team, but can't be the *investigator* or *insider*.

##### Products
- **Suspects list**: A list containing parts of the code that might need to be refactored. Every item relates to a refactor action to be taken over the code. Some of them will be taken during the event and other will be left to the future.
- **10 Most Wanted**: A list elaborated by all the participants at the end of the event. The list contains the top 10 technical debt items not attended at the code or at the system as a whole.

##### Resources
- Computer with the editable and testable source code + code review tool
- Projector or television + 2 sets of keyboard/mouse (for the *insiders* and *non-insiders*)

## Dynamics (01:50  to 2 hours)

### Opening (5 minutes)
The **deputy** introduces the **goal**, **participants** and the **dynamics** of the event.
.
### Stage 1 (10 minutes) – Field Report (Internal view)
The **insiders** brief about the code that will be investigated. At this stage, they can already add *suspects* to the **suspect list** and report known issues. The report must include an overview of the system architecture, technological issues and other issues kinds of important issues to the event.

### Stage 2 (15 minutes) – Investigation and planning (External view)
Based on the **field report**, the **Investigator** will check the code to confirm the **insiders** suspicions and look for new *suspects*. The investigation continues as the **investigator** exposes his views with the assistance from the **SWAT** team at any moment. 
At the stage's end, the participants discuss the relevant issues and sort the **suspect list** (version 1) to be used on the next stage.

### Stage 3 (60 minutes) – Tactical Operation (Combined Refactor)
Based on the **suspect list**, the refactor actions over the code begins. The **SWAT** team take turns operating the computer to actually refactor the code, while explaining what going on and their reasons. Later on, the **insiders** also take turns to conclude the actions started and explained by the **SWAT**. This cycle is repeated for the duration of the stage. During this time, the **suspect list** can be modified or reprioritized. Ever non-solved items will be taken to a *next mission*.

### Stage 4 (10 - 20 Minutes) – Aftermath (Report)
The **investigator** conduct the consolidation and conclusion of the **suspect list**, writing down all the items that were effectively solved and then the ones left for a future opportunity. Based on this list, the **insiders** briefing and the **SWAT** remarks, the **investigator** elaborates the **10 most wanted** list.

### Closure (10 Minutes) – Retrospective
The **deputy** closes the event asking for feedback from the participants.

## Expected results
- The original source code is now refactorated and improuved
-Final version of the **suspect list** with taken and future actions
-Exchange of experiences, knowlodge, standarts and best pratices among the participants
-**10 most wanted** list with the main system's technical debt items 

## Final score
1. Total of *suspects* detained (solved items)
2. **Crime rate** variation index (variation of the metrics calculated by the code review tool)