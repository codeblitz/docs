# CodeBlitz - Philosophy

**CodeBlitz** is based on the **collaboration** of two different teams to a common goal. It differs from a zero-sum game as both teams seek to review and improve the source code **together**, with the possibility to **exchange** experiences and knowledge during the process. Thus, CodeBlitz can be considered a **cooperative game** where all the player play *against the board*. The philosophy and values of the game can be expressed on these **six principles**:

**Collaboration** - the team that maintains the source code **IS NOT** the target of any questions, **only the code, the system and the technical debt** are under judgement. That's the main reason to designate the team as **insiders**. In most cases, they are the best source of information about the code and the general system conditions. Paying attention to this team feedbacks is vital to improving quality.

**Trust** - Issues found are not credited to the team, only the merit of discovering, register, solve and learn with the appointed items. This trust is obtained from the sponsors and managers by understanding that the resulting code and scores are part of a ludic process and **not** performance indexes. The original team must trust the process in order to bring their biggest real problems to the event. The quality of the information brought by the **insiders**  is the biggest impact factor to the results.

**Continuous learning** - CodeBlitz events are nothing but a part in a long process to craft a good software. Although there are many books about refactoring, there is no other way to achieve a high technical level about it without lots of practice. Real world projects don't usually allow the luxury of investing time in practice. Refactoring is perspiration.

**Mutual Learning** - It's expected that the invited team brings knowledge and experience in several aspects. However, in several other aspects, the original team can be more trained. Thus, even if there are two developers group, there is no hierarchy of knowledge nor the expectation that only one of the teams might learn something new.

**Motivation** - Participating in the event does not assure the learning just as any refactor assure improvement. Quality needs will.

**Excellence** - Continuous collaborative learning associated with individual commitment pave the way to excellence. Excellence generates value through productivity and customer satisfaction. Excellence is a goal and a means.